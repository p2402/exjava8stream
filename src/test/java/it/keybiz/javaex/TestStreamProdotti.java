package it.keybiz.javaex;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.Assertions.offset;
import static org.junit.Assert.assertEquals;

public class TestStreamProdotti {
    StreamProdotti sl;

    @Before
    public void setUp() {
        sl = new StreamProdotti();
    }

    @After
    public void tearDown() {
        Magazziniere.reset();
    }
    
    @Test
    public void test_generaLibri() {
        List<Prodotti> list = sl.generaListaProdotti(10);
        assertThat(list)
                .hasSize(10)
                .doesNotContainNull();
    }

    @Test
    public void test_contaArticoloBellezza() {
        List<Prodotti> list = sl.generaListaProdotti(10);
        long cnt = sl.contaProdottiDiBellezza(list);
        assertEquals(3, cnt);
    }

    @Test
    public void test_prezzoCompresoTra12e15() {
        List<Prodotti> inList = sl.generaListaProdotti(5);
        List<Prodotti> outList = sl.prezzoCompresoTra12e15(inList);

        assertThat(outList)
            .allMatch(x -> x.getPrezzo() >= 12)
            .allMatch(x -> x.getPrezzo() <= 15);
    }

    @Test
    public void test_creaListaProdottiAlimentariEBellezza() {
        List<Prodotti> inList = sl.generaListaProdotti(10);
        List<String> outList = sl.filtraListaProdottiAlimentariEBellezza(inList);
        
        assertThat(outList).containsOnly(
            "Biscotti", "Pane",
            "Profumo", "Cipria",
            "Rossetto");
    }

    @Test
    public void test_generaListaProdottiAlimentari() {
        List<Prodotti> list = sl.generaListaProdottiAlimentari(10);
        
        assertThat(list).hasSize(10);
        assertThat(list).allMatch(x -> x.getCategoria() == Prodotti.Categoria.ALIMENTARI);
    }

    @Test
    public void test_checkSePresenteBurningChrome() {
        List<Prodotti> list = sl.generaListaProdotti(10);

        boolean presente = sl.checkSePresenteBurningChrome(list);
        
        assertThat(presente).isFalse();
    }

    @Test
    public void test_sommaCosti_reduce() {
        List<Prodotti> list = sl.generaListaProdotti(5);

        int sum = sl.sommaCosti_reduce(list);

        assertThat(sum).isEqualTo(102);
    }

    @Test
    public void test_sommaCosti_sum() {
        List<Prodotti> list = sl.generaListaProdotti(5);

        int sum = sl.sommaCosti_sum(list);

        assertThat(sum).isEqualTo(102);
    }

    @Test
    public void test_sommaCostiInDollari() {
        final double EUR_USD = 1.12;
        List<Prodotti> list = sl.generaListaProdotti(5);
        
        double sum = sl.sommaCostiInDollari(EUR_USD, list);

        assertThat(sum).isEqualTo(102 * EUR_USD, offset(0.001));
    }

    @Test
    public void test_prodottoMenoCaroDa12InSu() {
        List<Prodotti> list = sl.generaListaProdotti(5);

        Optional<Prodotti> prodotti = sl.prodottoMenoCaroDa12InSu(list);

        if (prodotti.isPresent())
            assertThat(prodotti.get().getTitolo()).isEqualTo("Cipria");
        else
            fail("should be present");
    }

    @Test
    public void test_prodottoOrdinatiPerPrezzo() {
        List<Prodotti> inList = sl.generaListaProdotti(10);

        List<Prodotti> outList = sl.prodottoOrdinatiPerPrezzo(inList);

        assertThat(outList)
            .isSortedAccordingTo(Comparator.comparing(Prodotti::getPrezzo));
    }

    @Test
    public void test_generaPozioneHarryPotterDa15Euro() {
        List<Prodotti> list = sl.generaPozioneHarryPotterDa15Euro(7);

        // Qualche esempio di asserzione
        assertThat(list).hasSize(7);
        assertThat(list).allMatch(x -> x.getTitolo().matches("Harry Potter [1-7]"));
        assertThat(list).allMatch(x -> x.getCategoria() == Prodotti.Categoria.ALIMENTARI);
        assertThat(list).allMatch(x -> x.getPrezzo() == 15);
        assertThat(list.stream()
            .map(Prodotti::getTitolo)
            .distinct()
            .collect(Collectors.toList())).hasSize(7);
    }

    @Test
    public void test_mescolaLista() {
        List<Prodotti> list = sl.generaListaProdotti(5);

        list.forEach(System.out::println);
        
        List<Prodotti> shuffle = sl.mescolaLista(list);

        assertThat(shuffle).containsExactlyInAnyOrderElementsOf(list); 
    }

    @Test
    public void test_primoPiuCaroDelPrecedente() {
        List<Prodotti> list = sl.generaListaProdotti(3);

        list.forEach(System.out::println);

        Optional<Prodotti> prodotti = sl.primoPiuCaroDelPrecedente(list);
        
        assertThat(prodotti).isPresent();
        assertThat(prodotti.get().getTitolo()).isEqualTo("Biscotti");
    }

}
